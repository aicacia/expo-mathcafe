import { syncGenerators } from "./syncGenerators";
import { syncQuizzes } from "./syncQuizzes";

syncGenerators();
syncQuizzes();
