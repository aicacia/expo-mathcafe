# expo-mathcafe

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![site](https://img.shields.io/badge/www-mathcafe-blue.svg)](https://aicacia.gitlab.io/expo-mathcafe/)
[![pipelines](https://gitlab.com/aicacia/expo-mathcafe/badges/master/pipeline.svg)](https://gitlab.com/aicacia/expo-mathcafe/-/pipelines)

## Getting Started

Install deps and start the dev server

```bash
npm install
npm start
```

Run in dev with electron for desktop

```bash
npm run desktop
```

## Tech

- [Expo](https://docs.expo.io/)/[React Native](https://reactnative.dev/docs/getting-started)
- [react-native-paper](https://callstack.github.io/react-native-paper/index.html)
- [Electron](https://www.electronjs.org/)
- [Icons](https://icons.expo.fyi/)
