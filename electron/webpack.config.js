const { withExpoWebpack } = require("@expo/electron-adapter");

module.exports = (config) => withExpoWebpack(config);
