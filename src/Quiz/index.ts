export { Input, IInputProps } from "./Input";
export { Question, IQuestionProps } from "./Question";
export { Quiz, IQuizProps } from "./Quiz";
